// Global animation variables
let hasDesktopNavAnimated = false;
let globalDefaultAnimationDuration = 1000;
const easeInMethod = 'easeInOutQuart';
const easeOutMethod = 'easeOutQuart';
const globalAnimationDelay = globalDefaultAnimationDuration - 100;

// Helper Functions

/**
 * Checks if user device is mobile.
 * Desktop browsers tend to not use 'window.orientation property
 */
function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1)
}

// Checks if an element has a certain class. Returns Bool.
function hasClass(element, cls) {
    return !!element.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'))
}

//toggles css property for all elements of a certain class.
function togglePropClass(cls, prop, inverse = false) {
    const allClassElements = document.getElementsByClassName(cls);
    for (let element of allClassElements) {
        if (inverse) {
            if (!hasClass(element, prop)) element.classList.toggle(prop);
        }
        else {
            if (hasClass(element, prop)) element.classList.toggle(prop);
        }
    }
}

// Animation Functions
function pageAnimation(event = 'swipeDown', isDesktop = false) {
    const targetElement = '#page-container';
    let animationOptions = {
        targets: targetElement,
        duration: globalDefaultAnimationDuration
    };

    // Mobile page-container animation
    if (!isDesktop) {
        let translateValue = ['-115%', 0];
        let easeMethod = easeInMethod;

        if (event === 'swipeUp') {
            translateValue = [0, '-115%'];
            easeMethod = easeOutMethod;
        }

        animationOptions['translateY'] = translateValue;
        animationOptions['easing'] = easeMethod;
    }
    // Desktop page-container animation
    else {
        let opacityValue = 1.0;
        let pageAnimateDelay = globalAnimationDelay;

        if (event === 'swipeUp') {
            opacityValue = 0.0;
            pageAnimateDelay = 300;
        }

        animationOptions['opacity'] = opacityValue;
        animationOptions['delay'] = pageAnimateDelay;
        animationOptions['easing'] = easeOutMethod;
    }

    // uses AnimeJS Library to animate/tween
    anime(animationOptions)
}

function navAnimation(event = 'swipeDown', isDesktop = false) {
    const targetElement = '#nav';
    const easeMethod = (event === 'swipeUp') ? easeOutMethod : easeInMethod;
    let translateValue = [];

    let animationOptions = {
        targets: targetElement,
        duration: globalDefaultAnimationDuration,
        easing: easeMethod
    };

    // Mobile '#nav' animation
    if (!isDesktop) {
        translateValue =
            (event === 'swipeUp') ? ['90vh', '-10vh'] : ['-10vh', '90vh'];
    }
    // Desktop 'n#av' animation
    else {
        /**
         *  if '#nav' is being reverse animated, it waits for the '#page-container'
         *  animation to finish first. Thus the delay.
         */
        let navAnimationDelay = 0;

        if ((event === 'swipeUp')) {
            translateValue = ['-60vh', '-90vh'];
        } else {
            translateValue = ['-90vh', '-60vh'];
            navAnimationDelay = globalAnimationDelay;
        }

        animationOptions['delay'] = navAnimationDelay;
    }

    animationOptions['translateY'] = translateValue;
    anime(animationOptions)
}

// Handlers
function navButtonHandler(button, page) {
    let clickedButton = document.getElementById(button);
    let selectedPage = document.getElementById(page);

    if (!hasClass(clickedButton, 'is-clicked')) {
        togglePropClass('menu-button', 'is-clicked');
        togglePropClass('page', 'is-invisible', true);

        clickedButton.classList.toggle('is-clicked');

        /**
         * This animation setup is only for desktop since it relies on a nav-button click listener;
         * which is only used on non-mobile.
         */

        if (!isMobileDevice() && !hasDesktopNavAnimated) {
            navAnimation('swipeUp', true);
            hasDesktopNavAnimated = !hasDesktopNavAnimated;
            pageAnimation('swipeDown', true);
        }

        selectedPage.classList.toggle('is-invisible');
    }
}

// On document load {main}
document.addEventListener("DOMContentLoaded", function () {
    const body = document.body;
    if (isMobileDevice()) {

        const pageSwipeListener = new Hammer(body);
        let pulled_up = false;

        pageSwipeListener.get('swipe').set({direction: Hammer.DIRECTION_ALL});
        pageSwipeListener.on('swipeup', function () {
            pageAnimation('swipeUp');
            navAnimation('swipeUp');

            pulled_up = !pulled_up;
        });

        pageSwipeListener.on('swipedown', function () {
            if (pulled_up) {
                pageAnimation();
                navAnimation();

                pulled_up = !pulled_up;
            }
        });

        const defaultButtonClicked = document.getElementById('who-button');
        defaultButtonClicked.classList.toggle('is-clicked');
    }

    else {
        const landing = document.getElementsByClassName('landing-cover')[0];
        // Reverses 'page-container' & 'nav' animations if landing area is clicked
        landing.addEventListener('click', function () {
            if (hasDesktopNavAnimated) {
                togglePropClass('menu-button', 'is-clicked');
                pageAnimation('swipeUp', true);
                hasDesktopNavAnimated = !hasDesktopNavAnimated;
                navAnimation('swipeDown', true);
            }
        }, false);
    }
});